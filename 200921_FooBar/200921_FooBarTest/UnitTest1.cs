using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using _200921_FooBar;
using System.Collections.Generic;

namespace _200921_FooBarTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestFooAdd()
        {
            var context = new FooBarContext();
            context.Add(new Foo() { Count = 10, FooId = 1 });
            context.Add(new Foo() { Count = 9, FooId = 2 });
            context.Add(new Foo() { Count = 11, FooId = 3 });
            context.SaveChanges();

            List<Foo> FoosWhererFooCount10 = context.Foos.Where(f => f.Count == 10).ToList();
            Assert.IsTrue(FoosWhererFooCount10 != null);
        }
    }
}
