﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace _200921_FooBar
{
    public class FooBarContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = D:\Git\Standard\repos\2020_5chit_sew_28_wagner_rene\200921_FooBar\200921_FooBar\FooBarDatabase.mdf; Integrated Security = True");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Foo> Foos { get; set; }
        public DbSet<Bar> Bars { get; set; }
    }
}
