﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using _201109_Asp_FooBar.Model;

namespace _201109_Asp_FooBar.Data
{
    public class _201109_Asp_FooBarContext : DbContext
    {
        public _201109_Asp_FooBarContext (DbContextOptions<_201109_Asp_FooBarContext> options)
            : base(options)
        {
        }

        public DbSet<_201109_Asp_FooBar.Model.Processor> Rectangle { get; set; }

        public DbSet<_201109_Asp_FooBar.Model.Monitor> Triangle { get; set; }
    }
}
