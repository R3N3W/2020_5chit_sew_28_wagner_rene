﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace _201109_Asp_FooBar.Model
{
    public class Processor:AEntity
    {
        [Required]
        public int ClockspeedMHz { get; set; }

        [Required]
        public int TurobMHz { get; set; }

        public Processor(int clockSpeed, int Turbo)
        {
            ClockspeedMHz = clockSpeed;
            TurobMHz = Turbo;
        }

        public Processor() { }
    }
}
