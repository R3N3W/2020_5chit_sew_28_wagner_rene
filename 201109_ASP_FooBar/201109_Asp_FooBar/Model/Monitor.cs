﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace _201109_Asp_FooBar.Model
{
    public class Monitor:AEntity
    {
        [Required]
        public int YResolution { get; set; }

        [Required]
        public int XResolution { get; set; }

        [Required]
        public int MSRP { get; set; }

        public Monitor(int y, int x, int m)
        {
            YResolution = y;
            XResolution = x;
            MSRP = m;
        }

        public Monitor() { }
    }
}
