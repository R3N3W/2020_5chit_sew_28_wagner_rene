﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using _201109_Asp_FooBar.Data;
using _201109_Asp_FooBar.Model;

namespace _201109_Asp_FooBar
{
    public class EditModel : PageModel
    {
        private readonly _201109_Asp_FooBar.Data._201109_Asp_FooBarContext _context;

        public EditModel(_201109_Asp_FooBar.Data._201109_Asp_FooBarContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Monitor Monitor { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Monitor = await _context.Triangle.FirstOrDefaultAsync(m => m.Id == id);

            if (Monitor == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Monitor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MonitorExists(Monitor.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MonitorExists(long id)
        {
            return _context.Triangle.Any(e => e.Id == id);
        }
    }
}
