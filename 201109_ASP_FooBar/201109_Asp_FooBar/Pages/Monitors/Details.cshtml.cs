﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using _201109_Asp_FooBar.Data;
using _201109_Asp_FooBar.Model;

namespace _201109_Asp_FooBar
{
    public class DetailsModel : PageModel
    {
        private readonly _201109_Asp_FooBar.Data._201109_Asp_FooBarContext _context;

        public DetailsModel(_201109_Asp_FooBar.Data._201109_Asp_FooBarContext context)
        {
            _context = context;
        }

        public Monitor Monitor { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Monitor = await _context.Triangle.FirstOrDefaultAsync(m => m.Id == id);

            if (Monitor == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
