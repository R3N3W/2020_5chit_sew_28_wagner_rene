﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using _201109_Asp_FooBar.Data;
using _201109_Asp_FooBar.Model;

namespace _201109_Asp_FooBar
{
    public class DeleteModel : PageModel
    {
        private readonly _201109_Asp_FooBar.Data._201109_Asp_FooBarContext _context;

        public DeleteModel(_201109_Asp_FooBar.Data._201109_Asp_FooBarContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Monitor Monitor { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Monitor = await _context.Triangle.FirstOrDefaultAsync(m => m.Id == id);

            if (Monitor == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Monitor = await _context.Triangle.FindAsync(id);

            if (Monitor != null)
            {
                _context.Triangle.Remove(Monitor);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
