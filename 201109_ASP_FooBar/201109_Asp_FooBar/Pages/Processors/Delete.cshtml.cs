﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using _201109_Asp_FooBar.Data;
using _201109_Asp_FooBar.Model;

namespace _201109_Asp_FooBar
{
    public class DeleteModel : PageModel
    {
        private readonly _201109_Asp_FooBar.Data._201109_Asp_FooBarContext _context;

        public DeleteModel(_201109_Asp_FooBar.Data._201109_Asp_FooBarContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Processor Processor { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Processor = await _context.Rectangle.FirstOrDefaultAsync(m => m.Id == id);

            if (Processor == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Processor = await _context.Rectangle.FindAsync(id);

            if (Processor != null)
            {
                _context.Rectangle.Remove(Processor);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
