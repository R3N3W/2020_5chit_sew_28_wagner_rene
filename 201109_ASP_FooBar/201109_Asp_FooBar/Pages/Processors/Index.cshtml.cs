﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using _201109_Asp_FooBar.Data;
using _201109_Asp_FooBar.Model;

namespace _201109_Asp_FooBar
{
    public class IndexModel : PageModel
    {
        private readonly _201109_Asp_FooBar.Data._201109_Asp_FooBarContext _context;

        public IndexModel(_201109_Asp_FooBar.Data._201109_Asp_FooBarContext context)
        {
            _context = context;
        }

        public IList<Processor> Processor { get;set; }

        public async Task OnGetAsync()
        {
            Processor = await _context.Rectangle.ToListAsync();
        }
    }
}
