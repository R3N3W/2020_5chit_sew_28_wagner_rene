﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV1
{
    public class VeggieBurger : Burger
    {
        public VeggieBurger(EBreadType breadType, List<ETopping> toppings) : base(breadType, toppings)
        {
            BurgerType = EBurgerType.VEGGIE;
            SetPrice();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Veggieburger");
            //sb.Append(base.GetIngredientInfo());
            return sb.ToString();
        }
    }
}
