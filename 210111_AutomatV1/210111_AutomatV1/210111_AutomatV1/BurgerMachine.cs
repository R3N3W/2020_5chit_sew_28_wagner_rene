﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _210111_AutomatV1
{
    public enum EBurgerType { MCDOUBLE, DOUBLECHEESE, BIGMAC, VEGGIE, CHEESEBURGER }
    public enum EBreadType { SMALL, MEDIUM, LARGE }
    public enum ETopping { SALAD_LEAF, SAUCE, CHEESE_SLICE, TOMATO_SLICE, PATTY, VEGGIE_PATTY }
    public class BurgerMachine
    {
        private int amountBread = 0;
        private int amountSaladSlice = 0;
        private int amountSauce = 0;
        private int amountCheeseSlice = 0;
        private int amountTomatoSlice = 0;
        private int amountPatty = 0;
        private int amountVeggiePatty = 0;

        private int amountBigMac = 0;
        private int amountCheeseBurger = 0;
        private int amountMcDouble = 0;
        private int amountDoubleCheeseBurger = 0;
        private int amountVeggieBurger = 0;
        public string RefillAll(int amount)
        {
            amountBread += amount;
            amountSaladSlice += amount;
            amountSauce += amount;
            amountCheeseSlice += amount;
            amountTomatoSlice += amount;
            amountPatty += amount;
            amountVeggiePatty += amount;
            amountBigMac += amount;
            amountCheeseBurger += amount;
            amountMcDouble += amount;
            amountDoubleCheeseBurger += amount;
            amountVeggieBurger += amount;
            return ("Maschine um " + amount + "aufgefüllt");
        }
        public Burger ProduceBurger(EBurgerType burgerType, List<ETopping> toppings)
        {
            Burger b;
            switch (burgerType)
            {
                case EBurgerType.BIGMAC:
                    toppings.AddRange(new List<ETopping>() { ETopping.PATTY, ETopping.CHEESE_SLICE, ETopping.SALAD_LEAF, ETopping.SAUCE });
                    b = new BigMac(EBreadType.LARGE, toppings);
                    break;
                case EBurgerType.CHEESEBURGER:
                    toppings.AddRange(new List<ETopping>() { ETopping.PATTY, ETopping.CHEESE_SLICE });
                    b = new CheeseBurger(EBreadType.SMALL, toppings);
                    break;
                case EBurgerType.DOUBLECHEESE:
                    toppings.AddRange(new List<ETopping>() { ETopping.PATTY, ETopping.CHEESE_SLICE, ETopping.CHEESE_SLICE });
                    b = new DoubleCheeseBurger(EBreadType.MEDIUM, toppings);
                    break;
                case EBurgerType.MCDOUBLE:
                    toppings.AddRange(new List<ETopping>() { ETopping.PATTY, ETopping.PATTY, ETopping.SALAD_LEAF, ETopping.TOMATO_SLICE });
                    b = new MCDouble(EBreadType.LARGE, toppings);
                    break;
                case EBurgerType.VEGGIE:
                    toppings.AddRange(new List<ETopping>() { ETopping.VEGGIE_PATTY, ETopping.SALAD_LEAF });
                    b = new VeggieBurger(EBreadType.MEDIUM, toppings);
                    break;
                default:
                    throw new Exception("No Burger");
                    break;
            }
            UseSupplies(b);
            return b;
        }
        public void UseSupplies(Burger burger)
        {
            //if (burger.Toppings.Where(x => (int)x == 2).Count() > 1)
            //    throw new Exception(amountCheeseSlice.ToString());
            switch(burger.BurgerType)
            {
                case EBurgerType.BIGMAC:
                    if (amountBigMac > 0)
                        amountBigMac--;
                    else throw new OutOfBurgerException(burger);
                    break;
                case EBurgerType.CHEESEBURGER:
                    if (amountCheeseBurger > 0)
                        amountCheeseBurger--;
                    else throw new OutOfBurgerException(burger);
                    break;
                case EBurgerType.DOUBLECHEESE:
                    if (amountDoubleCheeseBurger > 0)
                        amountDoubleCheeseBurger--;
                    else throw new OutOfBurgerException(burger);
                    break;
                case EBurgerType.MCDOUBLE:
                    if (amountMcDouble > 0)
                        amountMcDouble--;
                    else throw new OutOfBurgerException(burger);
                    break;
                case EBurgerType.VEGGIE:
                    if (amountVeggieBurger > 0)
                        amountVeggieBurger--;
                    else throw new OutOfBurgerException(burger);
                    break;
            }

            if (amountBread > 0)
                amountBread--;
            else
                throw new OutOfBreadException();

            for (int i = 0; i < burger.Toppings.Count; i++)
            {
                switch (burger.Toppings[i])
                {
                    case ETopping.PATTY:
                        if (amountPatty > 0)
                            amountPatty--;
                        else
                            throw new OutOfPattysException();
                        break;
                    case ETopping.VEGGIE_PATTY:
                        if (amountVeggiePatty > 0)
                            amountVeggiePatty--;
                        else
                            throw new OutOfVeggiePattyException();
                        break;
                    case ETopping.SALAD_LEAF:
                        if (amountSaladSlice > 0)
                            amountSaladSlice--;
                        else
                            throw new OutOfSaladException();
                        break;
                    case ETopping.SAUCE:
                        if (amountSauce > 0)
                            amountSauce--;
                        else
                            throw new OutOfSauceException();
                        break;
                    case ETopping.CHEESE_SLICE:
                        if (amountCheeseSlice > 0)
                            amountCheeseSlice--;
                        else
                            throw new OutOfCheeseException();
                        break;
                    case ETopping.TOMATO_SLICE:
                        if (amountTomatoSlice > 0)
                            amountTomatoSlice--;
                        else
                            throw new OutOfTomatosException();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
