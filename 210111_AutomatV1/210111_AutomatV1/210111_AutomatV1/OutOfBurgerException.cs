﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV1
{
    public class OutOfBurgerException : Exception
    {
        public OutOfBurgerException()
        {

        }
        public OutOfBurgerException(Burger b) : base("Kein " + b.GetType().Name + " verfügbar")
        {

        }
    }
}
