﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV1
{
    public class BigMac : Burger
    {
        public BigMac(EBreadType breadType, List<ETopping> toppings) : base(breadType, toppings)
        {
            BurgerType = EBurgerType.BIGMAC;
            SetPrice();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("BigMac");
            //sb.Append(base.GetIngredientInfo());
            return sb.ToString();
        }
    }
}
