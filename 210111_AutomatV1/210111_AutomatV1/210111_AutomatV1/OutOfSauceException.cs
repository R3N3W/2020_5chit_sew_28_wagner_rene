﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV1
{
    public class OutOfSauceException : Exception
    {
        public OutOfSauceException():base("Keine Sauce mehr")
        {

        }
    }
}
