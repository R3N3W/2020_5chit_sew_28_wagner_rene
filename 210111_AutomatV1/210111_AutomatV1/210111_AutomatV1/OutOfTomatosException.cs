﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV1
{
    public class OutOfTomatosException : Exception
    {
        public OutOfTomatosException():base("Keine Tomaten mehr")
        {

        }
    }
}
