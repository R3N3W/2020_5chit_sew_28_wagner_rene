﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV1
{
    public class OutOfSaladException : Exception
    {
        public OutOfSaladException() : base("kein Salat mehr")
        {

        }
    }
}
