﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV1
{
    public class Burger
    {
        public double Price { get; set; }
        public EBurgerType BurgerType { get; set; }
        public EBreadType BreadType { get; set; }
        public List<ETopping> Toppings { get; set; }

        public Burger(EBreadType breadType, List<ETopping> toppings)
        {
            this.BreadType = breadType;
            this.Toppings = toppings;
        }

        protected void SetPrice()
        {
            Price = 1 + Toppings.Count * 0.5;
            if (BreadType == EBreadType.MEDIUM)
                Price += 0.5;
            if (BreadType == EBreadType.LARGE)
                Price += 1;
        }
        public string GetIngredientInfo()
        {
            StringBuilder sb = new StringBuilder();
            switch (BreadType)
            {
                case EBreadType.SMALL:
                    sb.Append("Kleiner Burger ");
                    break;
                case EBreadType.MEDIUM:
                    sb.Append("Mittelgroßer Burger ");
                    break;
                case EBreadType.LARGE:
                    sb.Append("Großer Burger ");
                    break;
            }
            for(int i =0; i <= Toppings.Count; i++)
            {
                int pattyCount = 0;
                int veggiePattyCount = 0;
                foreach (ETopping ts in Toppings)
                {
                    if (ts == ETopping.PATTY)
                        pattyCount++;
                    if (ts == ETopping.VEGGIE_PATTY)
                        veggiePattyCount++;
                }

                sb.Append("mit ");
                if (pattyCount == 1)
                    sb.Append("einer Brulette, ");
                else if (pattyCount > 1)
                    sb.Append(pattyCount + "Bruletten, ");
                else if (veggiePattyCount == 1)
                    sb.Append("einer veganen Brulette, ");
                else if (veggiePattyCount > 1)
                    sb.Append(veggiePattyCount + "vegane Bruletten, ");
                switch (Toppings[i])
                {
                    case ETopping.SALAD_LEAF:
                        sb.Append("Salat");
                        break;
                    case ETopping.SAUCE:
                        sb.Append("Sauce");
                        break;
                    case ETopping.CHEESE_SLICE:
                        sb.Append("Käse");
                        break;
                    case ETopping.TOMATO_SLICE:
                        sb.Append("Tomaten");
                        break;
                    default:
                        break;
                }
                if (i < Toppings.Count -1)
                    sb.Append(", ");
                if (i < Toppings.Count)
                    sb.Append(" und ");
            }
            return sb.ToString();
        }
    }
}
