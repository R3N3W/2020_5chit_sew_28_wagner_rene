﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace _200928_WeaponsDepot
{
    public class WeaponContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = D:\Git\Standard\repos\2020_5chit_sew_28_wagner_rene\200928_WeaponsDepot\200928_WeaponsDepot\WeaponDepotDb.mdf; Integrated Security = True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //n:m Relation
            modelBuilder.Entity<GunMagazineJoinedTable>().HasKey(e => new { e.GunId, e.CartridgeId });
            //Shadow Property
            modelBuilder.Entity<Gun>().Property<DateTime>("CreatedDate");
        }
        public DbSet<AEntity> AEntities { get; set; }
        public DbSet<Gun> Guns { get; set; }
        public DbSet<Explosive_Device> Explosive_Devices { get; set; }
        public DbSet<Missile> Missiles { get; set; }
        public DbSet<Knife> Knives { get; set; }
        public DbSet<Tank> Tanks { get; set; }
        public DbSet<Magazine> Magazines { get; set; }
        public DbSet<Cartridge> Cartridges { get; set; }

        internal void Seed()
        {
            List<Gun> guns = new List<Gun>();
            List<Explosive_Device> explosives = new List<Explosive_Device>();
            List<Missile> missiles = new List<Missile>();
            List<Knife> knives = new List<Knife>();
            List<Tank> tanks = new List<Tank>();


        }
    }
}
