﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace _200928_WeaponsDepot
{
    public class AEntity
    {
        [Key]
        public int Id { get; set; }
        [Timestamp]
        public byte[] Timestamp { get; set; }
        protected AEntity ()
        {
            
        }
    }
    //public enum EWeapons_Category
    //{
    //    Automatic_Gun,
    //    Semi_Automatic_Gun,
    //    Manual_Gun,
    //    Melee_Weapon,
    //    Throwing_Weapon
    //}
    public enum EBlade_Geometry
    {
        NormalBlade,
        Trailing_Point_Blade,
        Clip_Point_Blade,
        Drop_Point_Blade,
        Spear_Point_Blade,
        Needle_Point_Blade,
        Spray_Point_Blade,
        Tantoe_Blade,
        Sheepfoot_Blade,
        Wharncliffe_Blade,
        Ulu
    }
    public enum ECountries
    {
        USA,
        Austria,
        Germany,
        France,
        Canada,
        GB
    }
    public class Gun : AEntity
    {
        [Required]
        public string Title { get; set; }
        public DateTime ProductionDate { get; set; }
        public bool IsCivilWeapon { get; set; }
        public int Caliber { get; set; }
        public Magazine Magazine { get; set; }
        public Gun (string title, DateTime productionDate, bool isCivilWeapon, int caliber, Magazine magazine)
        {
            this.Title = title;
            this.ProductionDate = productionDate;
            this.IsCivilWeapon = isCivilWeapon;
            this.Caliber = caliber;
            this.Magazine = magazine;
        }
    }
    //Table Per Hierarchy Inheritances
    public class Rifle : Gun
    {
        public Rifle(string title, DateTime productionDate, bool isCivilWeapon, int caliber, Magazine magazine) : base(title, productionDate, isCivilWeapon, caliber,magazine)
        {

        }
    }
    public class Pistol : Gun
    {
        public Pistol(string title, DateTime productionDate, bool isCivilWeapon, int caliber, Magazine magazine) : base(title, productionDate, isCivilWeapon, caliber, magazine)
        {

        }
    }
    //
    public class Magazine : AEntity
    {
        [Required]
        public string Title { get; set; }
        public int HoldingCapacity { get; set; }
        public Magazine(string title, int holdingCapacity)
        {
            Title = title;
            HoldingCapacity = holdingCapacity;
        }
    }
    public class Cartridge :AEntity
    {
        public long Size { get; set; }
        public Cartridge(int size)
        {
            Size = size;
        }
    }
    [Table("GunMagazineJoinedTable")]
    public class GunMagazineJoinedTable
    {
        [Required]
        public Gun Gun { get; set; }
        [Required]
        [ForeignKey("Gun")]
        public long GunId { get; set; }

        [Required]
        public Cartridge Cartridge { get; set; }
        [Required]
        [ForeignKey("Cartridge")]
        public long CartridgeId { get; set; }
    }
    public class Explosive_Device : AEntity
    {
        [Required]
        public string Title { get; set; }
        public int Mass { get; set; }
        public ECountries CountryOfOrigin {get;set;}
        public DateTime ProductionDate { get; set; }
        public Explosive_Device(string title, int mass, ECountries countryOfOrigin, DateTime productionDate)
        {
            Title = title;
            Mass = mass;
            CountryOfOrigin = countryOfOrigin;
            ProductionDate = productionDate;
        }
    }
    public class Missile : AEntity
    {
        [Required]
        public string Title { get; set; }
        public int Mass { get; set; }
        public ECountries CountryOfOrigin { get; set; }
        public DateTime ProductionDate { get; set; }
        public Missile(string title, int mass, ECountries countryOfOrigin, DateTime productionDate)
        {
            Title = title;
            Mass = mass;
            CountryOfOrigin = countryOfOrigin;
            ProductionDate = productionDate;
        }
    }
    public class Knife :AEntity
    {
        [Required]
        public string Title { get; set; }
        public bool IsCivilWeapon { get; set; }
        public DateTime ProductionDate { get; set; }
        public EBlade_Geometry Blade_Geometry { get; set; }
        public Knife(string title, bool isCivilWeapon, DateTime productionDate, EBlade_Geometry blade_Geometry)
        {
            Title = title;
            IsCivilWeapon = isCivilWeapon;
            ProductionDate = productionDate;
            Blade_Geometry = blade_Geometry;
        }
    }
    public class Tank : AEntity
    {
        [Required]
        public string Title { get; set; }
        public DateTime ProductionDate { get; set; }
        public int TopSpeed { get; set; }
        public ECountries CountryOfOrigin { get; set; }
        public Tank (string title, DateTime productionDate, int topSpeed, ECountries countryOfOrigin)
        {
            this.Title = title;
            this.ProductionDate = productionDate;
            this.TopSpeed = topSpeed;
            this.CountryOfOrigin = countryOfOrigin;
        }
    }
    //Instructs the compiler to use Table Per Type
    [Table("ArmoredRecoveryVehicles")]
    public class ArmoredRecoveryVehicle : Tank
    {
        public int PowerOfCrane { get; set; }
        public ArmoredRecoveryVehicle (int powerOfCrane, string title, DateTime productionDate, int topSpeed, ECountries countryOfOrigin) : base(title,productionDate, topSpeed, countryOfOrigin)
        {
            this.PowerOfCrane = powerOfCrane;
        }
    }
    [Table("ArmoredVehicleLaunchedBridges")]
    public class ArmoredVehicleLaunchedBridge : Tank
    {
        public int LengthOfBridge { get; set; }
        public ArmoredVehicleLaunchedBridge(int lengthOfBridge, string title, DateTime productionDate, int topSpeed, ECountries countryOfOrigin) : base(title, productionDate, topSpeed, countryOfOrigin)
        {
            this.LengthOfBridge = lengthOfBridge;
        }
    }
    //
}
