﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace _200928_WeaponsDepot.Migrations
{
    public partial class CreateFooBarDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AEntities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Discriminator = table.Column<string>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    HoldingCapacity = table.Column<int>(nullable: true),
                    Explosive_Device_Title = table.Column<string>(nullable: true),
                    Mass = table.Column<int>(nullable: true),
                    CountryOfOrigin = table.Column<int>(nullable: true),
                    ProductionDate = table.Column<DateTime>(nullable: true),
                    Weapons_Category = table.Column<int>(nullable: true),
                    Knife_Title = table.Column<string>(nullable: true),
                    IsCivilWeapon = table.Column<bool>(nullable: true),
                    Knife_ProductionDate = table.Column<DateTime>(nullable: true),
                    blade_Geometry = table.Column<int>(nullable: true),
                    Knife_Weapons_Category = table.Column<int>(nullable: true),
                    Missile_Title = table.Column<string>(nullable: true),
                    Missile_Mass = table.Column<int>(nullable: true),
                    Missile_CountryOfOrigin = table.Column<int>(nullable: true),
                    Missile_ProductionDate = table.Column<DateTime>(nullable: true),
                    Rifle_Title = table.Column<string>(nullable: true),
                    Rifle_ProductionDate = table.Column<DateTime>(nullable: true),
                    Rifle_IsCivilWeapon = table.Column<bool>(nullable: true),
                    Caliber = table.Column<int>(nullable: true),
                    Rifle_Weapons_Category = table.Column<int>(nullable: true),
                    cartridgeId = table.Column<int>(nullable: true),
                    Tank_Title = table.Column<string>(nullable: true),
                    Tank_ProductionDate = table.Column<DateTime>(nullable: true),
                    TopSpeed = table.Column<int>(nullable: true),
                    Tank_CountryOfOrigin = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AEntities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AEntities_AEntities_cartridgeId",
                        column: x => x.cartridgeId,
                        principalTable: "AEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AEntities_cartridgeId",
                table: "AEntities",
                column: "cartridgeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AEntities");
        }
    }
}
