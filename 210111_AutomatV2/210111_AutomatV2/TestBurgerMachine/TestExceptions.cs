﻿using _210111_AutomatV2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestBurgerMachine
{
    [TestClass]
    public class TestExceptions
    {
        [TestMethod]
        public void TestOutOfBigMacException()
        {
            BurgerMachine bm = new BurgerMachine();
            var e = Assert.ThrowsException<OutOfBurgerException>(() => bm.ProduceBurger(EBurgerType.BIGMAC, new List<ETopping>() { ETopping.TOMATO_SLICE}));
            Assert.AreEqual("Kein BigMac verfügbar", e.Message);
        }
        [TestMethod]
        public void TestOutOfCheeseBurgerException()
        {
            BurgerMachine bm = new BurgerMachine();
            var e = Assert.ThrowsException<OutOfBurgerException>(() => bm.ProduceBurger(EBurgerType.CHEESEBURGER, new List<ETopping>() { ETopping.TOMATO_SLICE }));
            Assert.AreEqual("Kein CheeseBurger verfügbar", e.Message);
        }
        [TestMethod]
        public void TestOutOfDoubleCheeseburgerExcetpion()
        {
            BurgerMachine bm = new BurgerMachine();
            var e = Assert.ThrowsException<OutOfBurgerException>(() => bm.ProduceBurger(EBurgerType.DOUBLECHEESE, new List<ETopping>() { ETopping.TOMATO_SLICE }));
            Assert.AreEqual("Kein DoubleCheeseBurger verfügbar", e.Message);
        }
        [TestMethod]
        public void TestOutOfMCDoubleException()
        {
            BurgerMachine bm = new BurgerMachine();
            var e = Assert.ThrowsException<OutOfBurgerException>(() => bm.ProduceBurger(EBurgerType.MCDOUBLE, new List<ETopping>() { ETopping.TOMATO_SLICE }));
            Assert.AreEqual("Kein MCDouble verfügbar", e.Message);
        }
        [TestMethod]
        public void TestOutOfVeggieBurgerException()
        {
            BurgerMachine bm = new BurgerMachine();
            var e = Assert.ThrowsException<OutOfBurgerException>(() => bm.ProduceBurger(EBurgerType.VEGGIE, new List<ETopping>() { ETopping.TOMATO_SLICE }));
            Assert.AreEqual("Kein VeggieBurger verfügbar", e.Message);
        }
        [TestMethod]
        public void TestOutOfCheesexception()
        {
            BurgerMachine bm = new BurgerMachine();
            bm.RefillAll(1);
            var e = Assert.ThrowsException<OutOfCheeseException>(() => bm.ProduceBurger(EBurgerType.DOUBLECHEESE, new List<ETopping>()));
            Assert.AreEqual("Kein Käse mehr", e.Message);
        }
        [TestMethod]
        public void TestOutOfPattyexception()
        {
            BurgerMachine bm = new BurgerMachine();
            bm.RefillAll(1);
            var e = Assert.ThrowsException<OutOfPattysException>(() => bm.ProduceBurger(EBurgerType.DOUBLECHEESE, new List<ETopping>() { ETopping.PATTY}));
            Assert.AreEqual("Keine Brulletten mehr", e.Message);
        }
        [TestMethod]
        public void TestOutOfSaladexception()
        {
            BurgerMachine bm = new BurgerMachine();
            bm.RefillAll(1);
            var e = Assert.ThrowsException<OutOfSaladException>(() => bm.ProduceBurger(EBurgerType.DOUBLECHEESE, new List<ETopping>() { ETopping.SALAD_LEAF, ETopping.SALAD_LEAF }));
            Assert.AreEqual("kein Salat mehr", e.Message);
        }
        [TestMethod]
        public void TestOutOfSauceexception()
        {
            BurgerMachine bm = new BurgerMachine();
            bm.RefillAll(1);
            var e = Assert.ThrowsException<OutOfSauceException>(() => bm.ProduceBurger(EBurgerType.DOUBLECHEESE, new List<ETopping>() { ETopping.SAUCE, ETopping.SAUCE}));
            Assert.AreEqual("Keine Sauce mehr", e.Message);
        }
        [TestMethod]
        public void TestOutOfTomatosexception()
        {
            BurgerMachine bm = new BurgerMachine();
            bm.RefillAll(1);
            var e = Assert.ThrowsException<OutOfTomatosException>(() => bm.ProduceBurger(EBurgerType.DOUBLECHEESE, new List<ETopping>() { ETopping.TOMATO_SLICE, ETopping.TOMATO_SLICE }));
            Assert.AreEqual("Keine Tomaten mehr", e.Message);
        }
        [TestMethod]
        public void TestOutOfVeggiePattyexception()
        {
            BurgerMachine bm = new BurgerMachine();
            bm.RefillAll(1);
            var e = Assert.ThrowsException<OutOfVeggiePattyException>(() => bm.ProduceBurger(EBurgerType.VEGGIE, new List<ETopping>() { ETopping.VEGGIE_PATTY }));
            Assert.AreEqual("Keine Veggie Bruletten mehr", e.Message);
        }
    }
}
