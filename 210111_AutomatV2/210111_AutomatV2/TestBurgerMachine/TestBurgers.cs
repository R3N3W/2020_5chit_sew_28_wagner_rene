﻿using _210111_AutomatV2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestBurgerMachine
{
    [TestClass]
    public class TestBurgers
    {
        [TestMethod]
        public void TestBigMac()
        {
            BurgerMachine bm = new BurgerMachine();
            bm.RefillAll(2);
            Burger bigMac = bm.ProduceBurger(EBurgerType.BIGMAC, new List<ETopping>());
            Assert.AreEqual("BigMac", bigMac.ToString()) ;
        }
        [TestMethod]
        public void TestCheeseBurger()
        {
            BurgerMachine bm = new BurgerMachine();
            bm.RefillAll(1);
            Burger cheeseburger = bm.ProduceBurger(EBurgerType.CHEESEBURGER, new List<ETopping>());
            Assert.AreEqual("Cheeseburger", cheeseburger.ToString());
        }
        [TestMethod]
        public void TestDoubleCheeseBurger()
        {
            BurgerMachine bm = new BurgerMachine();
            bm.RefillAll(1);
            Burger dc = bm.ProduceBurger(EBurgerType.DOUBLECHEESE, new List<ETopping>());
            Assert.AreEqual("Double Cheeseburger", dc.ToString());
        }
        [TestMethod]
        public void TestMCDoublce()
        {
            BurgerMachine bm = new BurgerMachine();
            bm.RefillAll(2);
            Burger md = bm.ProduceBurger(EBurgerType.MCDOUBLE, new List<ETopping>());
            Assert.AreEqual("MCDouble", md.ToString());
        }
        [TestMethod]
        public void TestVeggieBurger()
        {
            BurgerMachine bm = new BurgerMachine();
            bm.RefillAll(1);
            Burger vb = bm.ProduceBurger(EBurgerType.VEGGIE, new List<ETopping>());
            Assert.AreEqual("Veggieburger", vb.ToString());
        }
    }
}
