﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV2
{
    public abstract class ABurgerBuilder
    {
        public abstract Burger BuildBurger();
        public abstract void SetBreadType();
        public abstract void AddToppings();
        public abstract void SetBurgerType();
        public abstract void SetBurgerPrice();
        public Burger Burger { get; protected set; }
        public ABurgerBuilder()
        {
            Burger = new Burger();
        }
    }
}
