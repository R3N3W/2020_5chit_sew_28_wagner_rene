﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV1.Exceptions
{
    public class OutOfIngredientException : Exception
    {
        public OutOfIngredientException() : base("Eine  Zutat fehlt")
        {

        }
    }
}
