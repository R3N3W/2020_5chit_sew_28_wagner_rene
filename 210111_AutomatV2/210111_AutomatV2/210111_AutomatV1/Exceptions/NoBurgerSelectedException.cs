﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV2
{
    public class NoBurgerSelectedException : Exception
    {
        public NoBurgerSelectedException() : base("Kein Burger ausgewählt")
        {

        }
    }
}
