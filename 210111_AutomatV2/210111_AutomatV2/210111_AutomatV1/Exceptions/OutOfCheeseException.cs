﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV2
{
    public class OutOfCheeseException : Exception
    {
        public OutOfCheeseException():base("Kein Käse mehr")
        {

        }
    }
}
