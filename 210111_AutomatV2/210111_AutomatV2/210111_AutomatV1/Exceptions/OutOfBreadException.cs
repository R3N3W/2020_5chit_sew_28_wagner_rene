﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV2
{
    public class OutOfBreadException : Exception
    {
        public OutOfBreadException():base("Keine Brötchen mehr")
        {

        }
    }
}
