﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV2
{
    public class OutOfVeggiePattyException : Exception
    {
        public OutOfVeggiePattyException() : base("Keine Veggie Bruletten mehr")
        {

        }
    }
}
