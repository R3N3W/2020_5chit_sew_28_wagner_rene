﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV2
{
    public class BigMacBuilder : ABurgerBuilder
    {
        public override Burger BuildBurger()
        {
            return Burger;
        }
        public override void AddToppings()
        {
            Burger.Toppings = new List<ETopping>() { ETopping.PATTY, ETopping.CHEESE_SLICE, ETopping.SALAD_LEAF, ETopping.SAUCE };
        }

        public override void SetBreadType()
        {
            Burger.BreadType = EBreadType.LARGE;
        }

        public override void SetBurgerType()
        {
            Burger.BurgerType = EBurgerType.BIGMAC;
        }
        public override void SetBurgerPrice()
        {
            Burger.SetPrice();
        }
    }
    public class CheeseBurgerBuilder : ABurgerBuilder
    {
        public override Burger BuildBurger()
        {
            return Burger;
        }
        public override void AddToppings()
        {
            Burger.Toppings = new List<ETopping>() { ETopping.PATTY, ETopping.CHEESE_SLICE };
        }

        public override void SetBreadType()
        {
            Burger.BreadType = EBreadType.SMALL;
        }

        public override void SetBurgerType()
        {
            Burger.BurgerType = EBurgerType.CHEESEBURGER;
        }
        public override void SetBurgerPrice()
        {
            Burger.SetPrice();
        }
    }
    public class DoubleCheeseBurgerBuilder : ABurgerBuilder
    {
        public override Burger BuildBurger()
        {
            return Burger;
        }
        public override void AddToppings()
        {
            Burger.Toppings = new List<ETopping>() { ETopping.PATTY, ETopping.CHEESE_SLICE, ETopping.CHEESE_SLICE };
        }

        public override void SetBreadType()
        {
            Burger.BreadType = EBreadType.MEDIUM;
        }

        public override void SetBurgerType()
        {
            Burger.BurgerType = EBurgerType.DOUBLECHEESE;
        }
        public override void SetBurgerPrice()
        {
            Burger.SetPrice();
        }
    }
    public class MCDoubleBuilder : ABurgerBuilder
    {
        public override Burger BuildBurger()
        {
            return Burger;
        }
        public override void AddToppings()
        {
            Burger.Toppings = new List<ETopping>() { ETopping.PATTY, ETopping.PATTY, ETopping.SALAD_LEAF, ETopping.TOMATO_SLICE };
        }

        public override void SetBreadType()
        {
            Burger.BreadType = EBreadType.LARGE;
        }

        public override void SetBurgerType()
        {
            Burger.BurgerType = EBurgerType.MCDOUBLE;
        }
        public override void SetBurgerPrice()
        {
            Burger.SetPrice();
        }
    }
    public class VeggieBurgerBuilder : ABurgerBuilder
    {
        public override Burger BuildBurger()
        {
            return Burger;
        }
        public override void AddToppings()
        {
            Burger.Toppings = new List<ETopping>() { ETopping.VEGGIE_PATTY, ETopping.SALAD_LEAF };
        }

        public override void SetBreadType()
        {
            Burger.BreadType = EBreadType.MEDIUM;
        }

        public override void SetBurgerType()
        {
            Burger.BurgerType = EBurgerType.VEGGIE;
        }
        public override void SetBurgerPrice()
        {
            Burger.SetPrice();
        }
    }
}
