﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV2
{
    public class CheeseBurger : Burger
    {
        public CheeseBurger(EBreadType breadType, List<ETopping> toppings) : base(breadType, toppings)
        {
            BurgerType = EBurgerType.CHEESEBURGER;
            SetPrice();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Cheeseburger");
            //sb.Append(base.GetIngredientInfo());
            return sb.ToString();
        }
    }
}
