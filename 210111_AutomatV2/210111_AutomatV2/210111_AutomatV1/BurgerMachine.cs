﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _210111_AutomatV2
{
    public enum EBurgerType { MCDOUBLE, DOUBLECHEESE, BIGMAC, VEGGIE, CHEESEBURGER }
    public enum EBreadType { SMALL, MEDIUM, LARGE }
    public enum ETopping { SALAD_LEAF, SAUCE, CHEESE_SLICE, TOMATO_SLICE, PATTY, VEGGIE_PATTY }
    public class BurgerMachine
    {
        private int amountBread = 0;
        private int amountSaladSlice = 0;
        private int amountSauce = 0;
        private int amountCheeseSlice = 0;
        private int amountTomatoSlice = 0;
        private int amountPatty = 0;
        private int amountVeggiePatty = 0;

        private int amountBigMac = 0;
        private int amountCheeseBurger = 0;
        private int amountMcDouble = 0;
        private int amountDoubleCheeseBurger = 0;
        private int amountVeggieBurger = 0;
        public Burger CurrentBurger { get; private set; }
        public string RefillAll()
        {
            amountBread += 1000;
            amountSaladSlice += 1000;
            amountSauce += 1000;
            amountCheeseSlice += 1000;
            amountTomatoSlice += 1000;
            amountPatty += 1000;
            amountVeggiePatty += 1000;
            amountBigMac += 1000;
            amountCheeseBurger += 1000;
            amountMcDouble += 1000;
            amountDoubleCheeseBurger += 1000;
            amountVeggieBurger += 1000;
            return ("Maschine um " + 1000 + "aufgefüllt");
        }

        public void ChooseBurger(ABurgerBuilder burgerBuilder)
        {
            burgerBuilder.SetBreadType();
            burgerBuilder.AddToppings();
            burgerBuilder.SetBurgerType();
            burgerBuilder.SetBurgerPrice();
            CurrentBurger = burgerBuilder.BuildBurger();
        }
        public Burger GetBurger()
        {
            if (CurrentBurger != null)
            {
                UseSupplies(CurrentBurger);
                Burger retBurger = CurrentBurger;
                CurrentBurger = null;
                return retBurger;
            }
            else
                throw new NoBurgerSelectedException();
        }
        private void UseSupplies(Burger burger)
        {
            //if (burger.Toppings.Where(x => (int)x == 2).Count() > 1)
            //    throw new Exception(amountCheeseSlice.ToString());
            switch(burger.BurgerType)
            {
                case EBurgerType.BIGMAC:
                    if (amountBigMac > 0)
                        amountBigMac--;
                    else throw new OutOfBurgerException(burger);
                    break;
                case EBurgerType.CHEESEBURGER:
                    if (amountCheeseBurger > 0)
                        amountCheeseBurger--;
                    else throw new OutOfBurgerException(burger);
                    break;
                case EBurgerType.DOUBLECHEESE:
                    if (amountDoubleCheeseBurger > 0)
                        amountDoubleCheeseBurger--;
                    else throw new OutOfBurgerException(burger);
                    break;
                case EBurgerType.MCDOUBLE:
                    if (amountMcDouble > 0)
                        amountMcDouble--;
                    else throw new OutOfBurgerException(burger);
                    break;
                case EBurgerType.VEGGIE:
                    if (amountVeggieBurger > 0)
                        amountVeggieBurger--;
                    else throw new OutOfBurgerException(burger);
                    break;
            }

            if (amountBread > 0)
                amountBread--;
            else
                throw new OutOfBreadException();

            for (int i = 0; i < burger.Toppings.Count; i++)
            {
                switch (burger.Toppings[i])
                {
                    case ETopping.PATTY:
                        if (amountPatty > 0)
                            amountPatty--;
                        else
                            throw new OutOfPattysException();
                        break;
                    case ETopping.VEGGIE_PATTY:
                        if (amountVeggiePatty > 0)
                            amountVeggiePatty--;
                        else
                            throw new OutOfVeggiePattyException();
                        break;
                    case ETopping.SALAD_LEAF:
                        if (amountSaladSlice > 0)
                            amountSaladSlice--;
                        else
                            throw new OutOfSaladException();
                        break;
                    case ETopping.SAUCE:
                        if (amountSauce > 0)
                            amountSauce--;
                        else
                            throw new OutOfSauceException();
                        break;
                    case ETopping.CHEESE_SLICE:
                        if (amountCheeseSlice > 0)
                            amountCheeseSlice--;
                        else
                            throw new OutOfCheeseException();
                        break;
                    case ETopping.TOMATO_SLICE:
                        if (amountTomatoSlice > 0)
                            amountTomatoSlice--;
                        else
                            throw new OutOfTomatosException();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
