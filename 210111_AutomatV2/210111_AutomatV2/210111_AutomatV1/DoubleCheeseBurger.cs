﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV2
{
    public class DoubleCheeseBurger : Burger
    {
        public DoubleCheeseBurger(EBreadType breadType, List<ETopping> toppings) : base(breadType, toppings)
        {
            BurgerType = EBurgerType.DOUBLECHEESE;
            SetPrice();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Double Cheeseburger");
            //sb.Append(base.GetIngredientInfo());
            return sb.ToString();
        }
    }
}
