﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210111_AutomatV2
{
    public class MCDouble : Burger
    {
        public MCDouble(EBreadType breadType, List<ETopping> toppings) : base(breadType, toppings)
        {
            BurgerType = EBurgerType.MCDOUBLE;
            SetPrice();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("MCDouble");
            //sb.Append(base.GetIngredientInfo());
            return sb.ToString();
        }
    }
}
