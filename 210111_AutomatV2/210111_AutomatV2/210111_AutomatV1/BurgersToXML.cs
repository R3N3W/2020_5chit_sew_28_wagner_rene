﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace _210111_AutomatV2
{
    public static class BurgersToXML
    {
        public static void WriteToFile(string path, List<Burger> burgers)
        {
            XDocument document = new XDocument(
                new XElement("burgers", from Burger b in burgers select new XElement(
                    "burger",
                    new XElement("BurgerType", b.BurgerType),
                    new XElement("BreadType", b.BreadType),
                    from ETopping et in b.Toppings select new XElement("Topping", et),
                    new XElement("Price", b.Price)
                    )));
            document.Save(path);
        }
    }
}
