﻿using _210111_AutomatV2;
using System;
using System.Collections.Generic;

namespace BurgerProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            BigMacBuilder bigMacBuilder = new BigMacBuilder();
            BurgerMachine bMachine = new BurgerMachine();
            bMachine.RefillAll();
            List<Burger> burgers = new List<Burger>();
            for (int i = 0; i < 100; i++)
            {
                bMachine.ChooseBurger(bigMacBuilder);
                burgers.Add(bMachine.GetBurger());
            }
            BurgersToXML.WriteToFile("BigMacs", burgers);

            CheeseBurgerBuilder cheeseBBuildeer = new CheeseBurgerBuilder();
            burgers = new List<Burger>();
            for (int i = 0; i < 100; i++)
            {
                bMachine.ChooseBurger(cheeseBBuildeer);
                burgers.Add(bMachine.GetBurger());
            }
            BurgersToXML.WriteToFile("CheeseBurgers", burgers);

            DoubleCheeseBurgerBuilder doublChBuilder = new DoubleCheeseBurgerBuilder();
            burgers = new List<Burger>();
            for (int i = 0; i < 100; i++)
            {
                bMachine.ChooseBurger(doublChBuilder);
                burgers.Add(bMachine.GetBurger());
            }
            BurgersToXML.WriteToFile("DoubleCheeseBurgers", burgers);

            MCDoubleBuilder mcDoubleBuilder = new MCDoubleBuilder();
            burgers = new List<Burger>();
            for (int i = 0; i < 100; i++)
            {
                bMachine.ChooseBurger(mcDoubleBuilder);
                burgers.Add(bMachine.GetBurger());
            }
            BurgersToXML.WriteToFile("MCDoubles", burgers);

            VeggieBurgerBuilder veggieBuilder = new VeggieBurgerBuilder();
            burgers = new List<Burger>();
            for (int i = 0; i < 100; i++)
            {
                bMachine.ChooseBurger(veggieBuilder);
                burgers.Add(bMachine.GetBurger());
            }
            BurgersToXML.WriteToFile("VeggieBurgers", burgers);
        }
    }
}
