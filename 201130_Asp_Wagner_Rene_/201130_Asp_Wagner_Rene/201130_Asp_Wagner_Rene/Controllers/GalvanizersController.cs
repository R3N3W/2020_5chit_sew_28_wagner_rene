﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using _201130_Asp_Wagner_Rene.Data;
using _201130_Asp_Wagner_Rene.Model;

namespace _201130_Asp_Wagner_Rene.Controllers
{
    public class GalvanizersController : Controller
    {
        private readonly CompanyContext _context;

        public GalvanizersController(CompanyContext context)
        {
            _context = context;
        }

        // GET: Galvanizers
        public async Task<IActionResult> Index()
        {
            return View(await _context.Galvanizers.ToListAsync());
        }

        // GET: Galvanizers/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var galvanizer = await _context.Galvanizers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (galvanizer == null)
            {
                return NotFound();
            }

            return View(galvanizer);
        }

        // GET: Galvanizers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Galvanizers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Id")] Galvanizer galvanizer)
        {
            if (ModelState.IsValid)
            {
                _context.Add(galvanizer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(galvanizer);
        }

        // GET: Galvanizers/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var galvanizer = await _context.Galvanizers.FindAsync(id);
            if (galvanizer == null)
            {
                return NotFound();
            }
            return View(galvanizer);
        }

        // POST: Galvanizers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Name,Id")] Galvanizer galvanizer)
        {
            if (id != galvanizer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(galvanizer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GalvanizerExists(galvanizer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(galvanizer);
        }

        // GET: Galvanizers/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var galvanizer = await _context.Galvanizers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (galvanizer == null)
            {
                return NotFound();
            }

            return View(galvanizer);
        }

        // POST: Galvanizers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var galvanizer = await _context.Galvanizers.FindAsync(id);
            _context.Galvanizers.Remove(galvanizer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GalvanizerExists(long id)
        {
            return _context.Galvanizers.Any(e => e.Id == id);
        }
    }
}
