﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using _201130_Asp_Wagner_Rene.Data;
using _201130_Asp_Wagner_Rene.Model;

namespace _201130_Asp_Wagner_Rene.Controllers
{
    public class MachineOperatorsController : Controller
    {
        private readonly CompanyContext _context;

        public MachineOperatorsController(CompanyContext context)
        {
            _context = context;
        }

        // GET: MachineOperators
        public async Task<IActionResult> Index()
        {
            return View(await _context.MachineOperators.ToListAsync());
        }

        // GET: MachineOperators/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var machineOperator = await _context.MachineOperators
                .FirstOrDefaultAsync(m => m.Id == id);
            if (machineOperator == null)
            {
                return NotFound();
            }

            return View(machineOperator);
        }

        // GET: MachineOperators/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MachineOperators/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Id")] MachineOperator machineOperator)
        {
            if (ModelState.IsValid)
            {
                _context.Add(machineOperator);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(machineOperator);
        }

        // GET: MachineOperators/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var machineOperator = await _context.MachineOperators.FindAsync(id);
            if (machineOperator == null)
            {
                return NotFound();
            }
            return View(machineOperator);
        }

        // POST: MachineOperators/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Name,Id")] MachineOperator machineOperator)
        {
            if (id != machineOperator.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(machineOperator);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MachineOperatorExists(machineOperator.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(machineOperator);
        }

        // GET: MachineOperators/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var machineOperator = await _context.MachineOperators
                .FirstOrDefaultAsync(m => m.Id == id);
            if (machineOperator == null)
            {
                return NotFound();
            }

            return View(machineOperator);
        }

        // POST: MachineOperators/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var machineOperator = await _context.MachineOperators.FindAsync(id);
            _context.MachineOperators.Remove(machineOperator);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MachineOperatorExists(long id)
        {
            return _context.MachineOperators.Any(e => e.Id == id);
        }
    }
}
