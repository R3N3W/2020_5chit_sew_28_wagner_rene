﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using _201130_Asp_Wagner_Rene.Data;
using _201130_Asp_Wagner_Rene.Model;

namespace _201130_Asp_Wagner_Rene.Controllers
{
    public class ADepartmentsController : Controller
    {
        private readonly CompanyContext _context;

        public ADepartmentsController(CompanyContext context)
        {
            _context = context;
        }

        // GET: ADepartments
        public async Task<IActionResult> Index()
        {
            return View(await _context.ADepartments.ToListAsync());
        }

        // GET: ADepartments/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aDepartment = await _context.ADepartments
                .FirstOrDefaultAsync(m => m.Id == id);
            if (aDepartment == null)
            {
                return NotFound();
            }

            return View(aDepartment);
        }

        // GET: ADepartments/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ADepartments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id")] ADepartment aDepartment)
        {
            if (ModelState.IsValid)
            {
                _context.Add(aDepartment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(aDepartment);
        }

        // GET: ADepartments/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aDepartment = await _context.ADepartments.FindAsync(id);
            if (aDepartment == null)
            {
                return NotFound();
            }
            return View(aDepartment);
        }

        // POST: ADepartments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id")] ADepartment aDepartment)
        {
            if (id != aDepartment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(aDepartment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ADepartmentExists(aDepartment.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(aDepartment);
        }

        // GET: ADepartments/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aDepartment = await _context.ADepartments
                .FirstOrDefaultAsync(m => m.Id == id);
            if (aDepartment == null)
            {
                return NotFound();
            }

            return View(aDepartment);
        }

        // POST: ADepartments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var aDepartment = await _context.ADepartments.FindAsync(id);
            _context.ADepartments.Remove(aDepartment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ADepartmentExists(long id)
        {
            return _context.ADepartments.Any(e => e.Id == id);
        }
    }
}
