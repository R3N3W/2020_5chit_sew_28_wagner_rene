﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using _201130_Asp_Wagner_Rene.Data;
using _201130_Asp_Wagner_Rene.Model;

namespace _201130_Asp_Wagner_Rene.Controllers
{
    public class SpecificMachineOperatorsController : Controller
    {
        private readonly CompanyContext _context;

        public SpecificMachineOperatorsController(CompanyContext context)
        {
            _context = context;
        }

        // GET: SpecificMachineOperators
        public async Task<IActionResult> Index()
        {
            return View(await _context.SpecificMachineOperators.ToListAsync());
        }

        // GET: SpecificMachineOperators/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var specificMachineOperator = await _context.SpecificMachineOperators
                .FirstOrDefaultAsync(m => m.Id == id);
            if (specificMachineOperator == null)
            {
                return NotFound();
            }

            return View(specificMachineOperator);
        }

        // GET: SpecificMachineOperators/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SpecificMachineOperators/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Id")] SpecificMachineOperator specificMachineOperator)
        {
            if (ModelState.IsValid)
            {
                _context.Add(specificMachineOperator);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(specificMachineOperator);
        }

        // GET: SpecificMachineOperators/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var specificMachineOperator = await _context.SpecificMachineOperators.FindAsync(id);
            if (specificMachineOperator == null)
            {
                return NotFound();
            }
            return View(specificMachineOperator);
        }

        // POST: SpecificMachineOperators/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Name,Id")] SpecificMachineOperator specificMachineOperator)
        {
            if (id != specificMachineOperator.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(specificMachineOperator);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SpecificMachineOperatorExists(specificMachineOperator.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(specificMachineOperator);
        }

        // GET: SpecificMachineOperators/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var specificMachineOperator = await _context.SpecificMachineOperators
                .FirstOrDefaultAsync(m => m.Id == id);
            if (specificMachineOperator == null)
            {
                return NotFound();
            }

            return View(specificMachineOperator);
        }

        // POST: SpecificMachineOperators/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var specificMachineOperator = await _context.SpecificMachineOperators.FindAsync(id);
            _context.SpecificMachineOperators.Remove(specificMachineOperator);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SpecificMachineOperatorExists(long id)
        {
            return _context.SpecificMachineOperators.Any(e => e.Id == id);
        }
    }
}
