﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _201130_Asp_Wagner_Rene.Model;
using Microsoft.EntityFrameworkCore;

namespace _201130_Asp_Wagner_Rene.Data
{
    public class CompanyContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Renew_Clevo\Documents\companyDb.mdf;Integrated Security=True;Connect Timeout=30");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DepartmentOfGalvanization>();
            modelBuilder.Entity<DepartmentOfProduction>();
            modelBuilder.Entity<DepartmentOfSoftwareEngineering>();
            modelBuilder.Entity<APerson>();
            base.OnModelCreating(modelBuilder);
        }
        public CompanyContext(DbContextOptions<CompanyContext> options) : base(options)
        {
            Seed();
        }
        public CompanyContext()
        { }
        public void Seed()
        {
            ADepartments.RemoveRange(ADepartments);
            Galvanizers.RemoveRange(Galvanizers);
            MachineOperators.RemoveRange(MachineOperators);
            SpecificMachineOperators.RemoveRange(SpecificMachineOperators);
            Programmers.RemoveRange(Programmers);
            SaveChanges();
            Galvanizers.Add(new Galvanizer { Name = "TestGalvanizer1" });
            Galvanizers.Add(new Galvanizer { Name = "TestGalvanizer2" });
            MachineOperators.Add(new MachineOperator { Name = "TestMachineOperator1" });
            MachineOperators.Add(new MachineOperator { Name = "TestMachineOperator2" });
            SpecificMachineOperators.Add(new SpecificMachineOperator { Name = "TestSpecificMachineOperator1", AuthorizationLevel = "Level1" });
            SpecificMachineOperators.Add(new SpecificMachineOperator { Name = "TestSpecificMachineOperator2", AuthorizationLevel = "Level2" });
            Programmers.Add(new Programmer { Name = "TestProgrammer1" });
            Programmers.Add(new Programmer { Name = "TestProgrammer2" });
            SaveChanges();
            ADepartments.Add(new DepartmentOfGalvanization { Employees = new List<APerson>(Galvanizers) });
            List<MachineOperator> mo = MachineOperators.ToList();
            mo.AddRange(SpecificMachineOperators.ToList());
            ADepartments.Add(new DepartmentOfProduction { Employees = new List<APerson> (mo) });
            ADepartments.Add(new DepartmentOfSoftwareEngineering { Employees = new List<APerson> (Programmers.ToList()) });
        }

        public DbSet<ADepartment> ADepartments { get; set; }
        public DbSet<Galvanizer> Galvanizers { get; set; }
        public DbSet<MachineOperator> MachineOperators { get; set; }
        public DbSet<SpecificMachineOperator> SpecificMachineOperators { get; set; }
        public DbSet<Programmer> Programmers { get; set; }
    }
}
