﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace _201130_Asp_Wagner_Rene.Model
{
    public abstract class APerson : AEntity
    {
        [Required]
        public string Name { get; set; }
    }
}
