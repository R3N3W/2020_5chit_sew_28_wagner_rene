﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace _201130_Asp_Wagner_Rene.Model
{
    [Table("SpecificMachineOperators")]
    public class SpecificMachineOperator : MachineOperator
    {
        [Required]
        public string AuthorizationLevel;
    }
}
