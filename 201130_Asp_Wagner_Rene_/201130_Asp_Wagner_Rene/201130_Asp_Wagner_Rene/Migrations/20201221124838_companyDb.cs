﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace _201130_Asp_Wagner_Rene.Migrations
{
    public partial class companyDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ADepartments",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Discriminator = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ADepartments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "APerson",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_APerson", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Galvanizers",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Galvanizers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Galvanizers_APerson_Id",
                        column: x => x.Id,
                        principalTable: "APerson",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MachineOperators",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MachineOperators", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MachineOperators_APerson_Id",
                        column: x => x.Id,
                        principalTable: "APerson",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Programmers",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Programmers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Programmers_APerson_Id",
                        column: x => x.Id,
                        principalTable: "APerson",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SpecificMachineOperators",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecificMachineOperators", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SpecificMachineOperators_MachineOperators_Id",
                        column: x => x.Id,
                        principalTable: "MachineOperators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ADepartments");

            migrationBuilder.DropTable(
                name: "Galvanizers");

            migrationBuilder.DropTable(
                name: "Programmers");

            migrationBuilder.DropTable(
                name: "SpecificMachineOperators");

            migrationBuilder.DropTable(
                name: "MachineOperators");

            migrationBuilder.DropTable(
                name: "APerson");
        }
    }
}
