using Microsoft.VisualStudio.TestTools.UnitTesting;
using _201130_Asp_Wagner_Rene.Data;
using _201130_Asp_Wagner_Rene.Model;
using System.Linq;

namespace UnitTestCompany
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            CompanyContext c = new CompanyContext();
            c.Add(new Galvanizer { Name = "ABCGalvanizer123" });
            c.SaveChanges();
            Assert.AreEqual(c.Galvanizers.Where(o => o.Name == "ABCGalvanizer123").Count(), 1);
            c.Remove(c.Galvanizers.Where(o => o.Name == "ABCGalvanizer123").FirstOrDefault());
            c.SaveChanges();
            Assert.AreEqual(c.Galvanizers.Where(o => o.Name == "ABCGalvanizer123").Count(), 0);
        }
    }
}
