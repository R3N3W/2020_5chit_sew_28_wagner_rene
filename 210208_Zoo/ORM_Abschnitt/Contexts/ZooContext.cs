﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ORM_Abschnitt.Contexts
{
    public class ZooContext : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\r.wagner\Documents\ZooDb.mdf;Integrated Security=True;Connect Timeout=30");
        }
    }
}
