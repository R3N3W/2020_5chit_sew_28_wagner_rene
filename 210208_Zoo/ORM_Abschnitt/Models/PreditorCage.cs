﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ORM_Abschnitt.Models
{
    [Table("PreditorCage")]
    public class PreditorCage : ACage
    {
        public Animal Predator { get; set; }
    }
}
