﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ORM_Abschnitt.Models
{
    [Table("FriendlyCage")]
    class FriendlyCage : ACage
    {
        public ICollection<Animal> FriendlyAnimals{ get; set; }
    }
}
