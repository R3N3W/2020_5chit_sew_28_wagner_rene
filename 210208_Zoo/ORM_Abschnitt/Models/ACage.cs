﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ORM_Abschnitt.Models
{
    public abstract class ACage : AEntity
    {
        public int CageCapacity { get; set; }
    }
}
