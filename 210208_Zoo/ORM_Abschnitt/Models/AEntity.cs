﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ORM_Abschnitt.Models
{
    public abstract class AEntity
    {
        [Key]
        public long Id { get; set; }
    }
}
