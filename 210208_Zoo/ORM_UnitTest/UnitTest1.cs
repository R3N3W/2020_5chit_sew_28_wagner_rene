using Microsoft.VisualStudio.TestTools.UnitTesting;
using ORM_Abschnitt;

namespace ORM_UnitTest {
    [TestClass]
    public class UnitTest1 {
        [TestMethod]
        public void TestMethod1() {
            MyClass i = new MyClass();
            string name = "Kurt";
            i.Name = name;
            Assert.AreEqual(name, i.Name);

        }
    }
}
